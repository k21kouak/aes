#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "aes.h"

static uint8_t sboxtab [256] = {
  //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
  0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
  0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
  0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
  0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
  0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
  0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
  0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
  0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
  0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
  0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
  0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
  0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
  0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
  0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
  0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
  0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

static uint8_t rcon [10] = {0x01, 0x02, 0x04 ,0x08 ,0x10 ,0x20, 0x40, 0x80,	0x1B,0x36};

#define getSBoxTabValue(state_current_value) (sboxtab[(state_current_value)])


void SubBytes ( uint8_t state [STATE_ROW_SIZE][STATE_COL_SIZE])
{
    for(int row_index = 0; row_index < STATE_ROW_SIZE; row_index++){
        for(int col_index = 0 ; col_index < STATE_COL_SIZE; col_index++){
            state[row_index][col_index] = getSBoxTabValue(state[row_index][col_index]);
        }
    }
}
void ShiftOneRow ( uint8_t state [ STATE_ROW_SIZE ][ STATE_COL_SIZE ], int row_index){
        uint8_t temp_first_value;
        
        temp_first_value = state[row_index][0];
        for(int col_index = 0 ; col_index < STATE_COL_SIZE - 1; col_index++){
            state[row_index][col_index] = state[row_index][col_index + 1];
        }
        state[row_index][3] = temp_first_value;
}

void ShiftRows ( uint8_t state [STATE_ROW_SIZE][STATE_COL_SIZE]){
   //Shit one time row 1
   ShiftOneRow(state, 1);
   //Shift 2 time row 2 
   ShiftOneRow(state, 2);
   ShiftOneRow(state, 2);
   //Shift 3 time row 3
   ShiftOneRow(state, 3);
   ShiftOneRow(state, 3);
   ShiftOneRow(state, 3);

}

uint8_t gmul ( uint8_t a, uint8_t b){
    uint8_t result;
    switch (a){
        case 0x02 :

            if(! ((b & 0x80) == 0x80)){ // If first bit is not equal to 1
                result = b << 1;
        }
       else { // first bit equal to 1
                result = b << 1;
                result ^= 0x1b;
            }
        break;

        default: // a == 0x03
           if(! ((b & 0x80) == 0x80)){ // If first bit is not equal to 1
                result = b << 1;
                result ^= b;
            }
            else { // first bit equal to 1
                result = b << 1;
                result ^= 0x1b;
                result ^= b;
            }
        break;
    }

    return  result;
}
void MixColumns ( uint8_t state [ STATE_ROW_SIZE ][ STATE_COL_SIZE ]){
    uint8_t temp_state [ STATE_ROW_SIZE ][ STATE_COL_SIZE ];

    for (int col_index = 0; col_index < STATE_COL_SIZE; col_index++){
        // 1 - element of current column
         temp_state[0][col_index] = (gmul(0x02, state[0][col_index])) ^ (gmul(0x03, state[1][col_index])) ^ state[2][col_index] ^ state[3][col_index];
        // 2 - element of current column
         temp_state[1][col_index] = state[0][col_index] ^ (gmul(0x02, state[1][col_index])) ^ (gmul(0x03, state[2][col_index])) ^ state[3][col_index];
        // 3 - element of current column
         temp_state[2][col_index] = state[0][col_index] ^ state[1][col_index] ^ (gmul(0x02, state[2][col_index])) ^ (gmul(0x03, state[3][col_index]));
        // 4 - element of current column
         temp_state[3][col_index] = (gmul(0x03, state[0][col_index])) ^ state[1][col_index] ^ state[2][col_index] ^ (gmul(0x02, state[3][col_index]));   
    }

    memcpy( state, temp_state, sizeof(temp_state));
}

// fill the first column of a given round key
void ColumnFill ( uint8_t roundkeys [][ STATE_ROW_SIZE ][ STATE_COL_SIZE ] , int round ){
    uint8_t temp [STATE_ROW_SIZE];
    
    //RotWord
    uint8_t temp_first_value;
    temp_first_value = roundkeys[round - 1][0][3];
    for (int index = 0 ; index < STATE_ROW_SIZE - 1; index++){
        temp[index] = roundkeys[round - 1][index + 1][3];
    }
    temp[3] = temp_first_value;


    //SubWord
    for(int index = 0; index < STATE_ROW_SIZE; index++){
            temp[index] = getSBoxTabValue(temp[index]);
    }


    //XOR with rcon
    // for (int index = 0; index < STATE_ROW_SIZE; index++)
    temp[0] ^= rcon[round - 1];

    //temp XOR first column of last key
    for (int index = 0; index < STATE_ROW_SIZE; index++)
        temp[index] ^=  roundkeys[round - 1][index][0];

    //Copy value to the first column
    for(int row_index = 0; row_index < STATE_ROW_SIZE; row_index++)
      roundkeys[round][row_index][0] = temp[row_index];
}

// fill the other 3 columns of a given round key
void OtherColumnsFill ( uint8_t roundkeys [][ STATE_ROW_SIZE ][ STATE_COL_SIZE ], int round ){
     uint8_t temp [STATE_ROW_SIZE];
     
     for(int key_col_index = 1 ; key_col_index < KEY_COL_SIZE ; key_col_index++){
         //Copy last  column in the current key
        for(int row_index = 0; row_index < STATE_ROW_SIZE; row_index++)
            temp[row_index] = roundkeys[round][row_index][key_col_index - 1];
         
        //XOR with temp
        for (int row_index = 0; row_index < STATE_ROW_SIZE; row_index++)
        roundkeys[round][row_index][key_col_index] = roundkeys[round - 1][row_index][key_col_index] ^ temp[row_index];
     }
}

//Generate keys for the rounds
void KeyGen ( uint8_t roundkeys [][ STATE_ROW_SIZE ][ STATE_COL_SIZE ], uint8_t master_key [STATE_ROW_SIZE ][ STATE_COL_SIZE ]){
    // Recopy the master_key in the first rounds keys array
    for (int col_index = 0; col_index < KEY_COL_SIZE; col_index++){
        for (int row_index = 0; row_index < KEY_ROW_SIZE; row_index++){
            roundkeys[0][row_index][col_index] = master_key[row_index][col_index];
        }
    }
    
    // Add others round keys ROUND_COUNT time
    for (int round_key_index = 1; round_key_index < ROUND_COUNT + 1; round_key_index++){
       ColumnFill(roundkeys,round_key_index);
       OtherColumnsFill(roundkeys, round_key_index);
    }

}

void GetRoundKey ( uint8_t roundkey [ STATE_ROW_SIZE ][ STATE_COL_SIZE ], uint8_t roundkeys [][STATE_ROW_SIZE ][ STATE_COL_SIZE ] , int round ){
    for (int row_index= 0; row_index < STATE_ROW_SIZE; row_index++)
        for(int col_index= 0; col_index < STATE_COL_SIZE; col_index++)
          roundkey[row_index][col_index] = roundkeys[round][row_index][col_index];
}
void AddRoundKey ( uint8_t state [ STATE_ROW_SIZE ][ STATE_COL_SIZE ] , uint8_t roundkey [STATE_ROW_SIZE ][ STATE_COL_SIZE ]){
    for (int row_index= 0; row_index < STATE_ROW_SIZE; row_index++)
        for(int col_index= 0; col_index < STATE_COL_SIZE; col_index++)
          state[row_index][col_index] ^= roundkey[row_index][col_index];

}

void MessageToState ( uint8_t state [ STATE_ROW_SIZE ][ STATE_COL_SIZE ], uint8_t message [DATA_SIZE ]){
for(int row_index = 0; row_index < STATE_ROW_SIZE; row_index++){
    for(int col_index = 0; col_index < STATE_COL_SIZE; col_index++){
        state[row_index][col_index] = message[row_index + 4 *col_index];
    }
}
}
void StateToMessage ( uint8_t message [ DATA_SIZE ], uint8_t state [ STATE_ROW_SIZE ][STATE_COL_SIZE ]){
for(int row_index = 0; row_index < STATE_ROW_SIZE; row_index++){
    for(int col_index = 0; col_index < STATE_COL_SIZE; col_index++){
       message[row_index + 4 *col_index] = state[row_index][col_index];
    }
}
}
void AESEncrypt ( uint8_t ciphertext [ DATA_SIZE ] , uint8_t plaintext [ DATA_SIZE ] , uint8_t key [DATA_SIZE ]){
    uint8_t state [ STATE_ROW_SIZE ][ STATE_COL_SIZE ];
    uint8_t roundkeys [ROUND_COUNT + 1][STATE_ROW_SIZE ][ STATE_COL_SIZE ];
    uint8_t master_key [ STATE_ROW_SIZE ][ STATE_COL_SIZE ];
    uint8_t roundkey [ STATE_ROW_SIZE ][ STATE_COL_SIZE ];
    
    MessageToState(master_key, key);
    KeyGen(roundkeys,master_key);

    MessageToState(state, plaintext);
    GetRoundKey(roundkey, roundkeys, 0);
    AddRoundKey(state, roundkey);

    int round;
    for(round = 1; round < ROUND_COUNT; round++){
        SubBytes(state);
        ShiftRows(state);
        MixColumns(state);

        GetRoundKey(roundkey, roundkeys, round);
        AddRoundKey(state, roundkey);


        custom_round_output(state, round);  // Remove after just for test
        
    }
    SubBytes(state);
    ShiftRows(state);
    GetRoundKey(roundkey, roundkeys, 10);
    AddRoundKey(state, roundkey);
    StateToMessage(ciphertext, state);
}



//TODO: Remove the custom prints function after. Just for my tests
void custom_round_output(uint8_t state[STATE_ROW_SIZE ][ STATE_COL_SIZE], int round){
    
    uint8_t message [ DATA_SIZE ];
    StateToMessage(message, state);

    printf("> round %d (output): ", round);
    for(int i=0;i < DATA_SIZE;i++)
         printf("%x ", message[i]);
    printf("\n");
}
void custom_pprint_input_ciphertext(uint8_t message [ DATA_SIZE ], char type) {
    if (type == 'i')
    {
        printf("input: ");
       for(int i=0;i < DATA_SIZE;i++)
         printf("%x ", message[i]);
       printf("\n\n");
    }
    else
    {
       printf("output: ");
       for(int i=0;i < DATA_SIZE;i++)
         printf("%x ", message[i]);
       printf("\n\n");

    }
}

void custom_pprint_2D_array(uint8_t state[STATE_ROW_SIZE ][ STATE_COL_SIZE]) {
  int i, j;
  for (i = 0; i < STATE_ROW_SIZE; ++i) {
    for (j = 0; j < STATE_COL_SIZE; ++j)
      printf("%x ", state[i][j]);
    printf("\n");
  }
}

int main(){
    uint8_t state [ STATE_ROW_SIZE ][ STATE_COL_SIZE ];
    uint8_t plaintext [ DATA_SIZE ] = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff};
    uint8_t key [ DATA_SIZE ] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};
    uint8_t ciphertext [ DATA_SIZE ];
   
    printf("Start : cipher....\n");
    custom_pprint_input_ciphertext(plaintext, 'i'); //Remove after
    MessageToState(state,plaintext);

    AESEncrypt (ciphertext, plaintext, key);
   
    custom_pprint_input_ciphertext(ciphertext, 'o'); //Remove after
    printf("End : cipher....\n");

    return 0;
}
